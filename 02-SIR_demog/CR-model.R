# Author: JR Mihaljevic
# Date: Wed Jan  9 14:34:38 2019
# Project Description:
## Assignment 2 testing
#-------------------------------------------------------
#-------------------------------------------------------
# Packages
library(tidyverse)
library(grid)
library(gridExtra)
library(deSolve)

options(dplyr.width = Inf, dplyr.print_max = 1e9)
#-------------------------------------------------------
#-------------------------------------------------------

lynx_hare = read_csv("./assignments/2-SIR_demog/hare_lynx_data.csv")

lynx_hare_plot = 
  lynx_hare %>%
  ggplot() +
  geom_point(aes(x = Year, y = Lynx), color = "red") +
  geom_line(aes(x = Year, y = Lynx), color = "red") +
  geom_point(aes(x = Year, y = Hare), color = "blue") +
  geom_line(aes(x = Year, y = Hare), color = "blue") +
  theme_classic() +
  theme(axis.text = element_text(size = 12, color = "black"),
        axis.title = element_text(size = 14, color = "black")) +
  labs(x = "Year", y = "Number of Pelts (thousands)")
  
quartz(height = 3, width = 5.5)
lynx_hare_plot
quartz.save(file = "./assignments/2-SIR_demog/Lynx-Hare.pdf", type = "pdf")

#-------------------------------------------------------
#-------------------------------------------------------

# Params:
# b: conversion factor (converts excess energy into reproductive output)
# r: per-capita growth rate of the resource
# rho: capture and consumption rate of resource
# m: maintenance requirement (death and physiological maintenance energy)

b = 0.1
r = 1.2
rho = 0.5
m = 0.2

# Equilibria:
R_eq = m/rho
X_eq = r/rho

# Jacobian
J = matrix(0, ncol = 2, nrow = 2)

# J[row, column]
J[1,1] = b*rho*R_eq - m*b
J[1,2] = b*rho*X_eq
J[2,1] = -rho * R_eq
J[2,2] = r - rho*X_eq

# eigenvalues:
cr_eigen = eigen(J)
cr_eigen$values

#-------------------------------------------------------
#-------------------------------------------------------
# The consumer-resource model: 

CRmod <- function(t, y, params) {
  with(as.list(c(y, params)), {
    
    dydt = rep(0, 2)
    
    dydt[1] = y[1] * b * (rho*y[2] - m)
    
    dydt[2] = r * y[2] - rho*y[1]*y[2]
    
    return(list(dydt))
  })
}

#-------------------------------------------------------
#-------------------------------------------------------
params = list(b = b,
              r = r,
              rho = rho,
              m = m)

# Inits
X_0 = 2.0
R_0 = 0.5
inits = c(X_0, R_0)

# times:
times_ode = seq(0, 200, by = 0.1)

# Solve ODE system:
out = ode(y = inits,
          times = times_ode,
          func = CRmod,
          parms = params,
          method = "ode45")

out = data.frame(out)
colnames(out) = c("time", "Consumer", "Resource")

#-------------------------------------------------------
#-------------------------------------------------------

# Plot:

plot(out$Consumer ~ out$time, type = "l")
abline(h = X_eq)
mean(out$Consumer); X_eq

plot(out$Resource ~ out$time, type = "l")
abline(h = R_eq)
mean(out$Resource); R_eq

plot(NA,NA, 
     xlim = c(0, max(times_ode)), xlab = "Time",
     ylim = c(0,max(out[,2:3])), ylab = "Density")
lines(out$Consumer ~ out$time, col = "black")
lines(out$Resource ~ out$time, col = "green")

#-------------------------------------------------------
#-------------------------------------------------------

# Create a phase diagram:

X_0 = seq(0.5, 3.0, length.out = 5)
R_0 = seq(0.1, 2.0, length.out = 5)

for(i in 1:length(X_0)){
  
  X_0_temp = X_0[i]
  
  for(j in 1:length(R_0)){
    
    R_0_temp = R_0[j]
  
    inits = c(X_0_temp, R_0_temp)
    
    out_temp = ode(y = inits,
                   times = times_ode,
                   func = CRmod,
                   parms = params,
                   method = "ode45")
    
    out_temp = data.frame(out_temp)
    colnames(out_temp) = c("time", "Consumer", "Resource")
    
    
    if(i == 1 & j == 1){
      plot(NA,NA, 
           xlim = c(0, 25), xlab = "Resource",
           ylim = c(0, 8), ylab = "Consumer")
    }
    
    lines(out_temp$Consumer ~ out_temp$Resource)
    
  }
  
}

# Add the neutrally stable, fixed point:
points(X_eq~R_eq, pch = 19, col = "red")
